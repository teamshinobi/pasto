package app.pasto.org.pasto.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import app.pasto.org.pasto.LoginActivity;
import app.pasto.org.pasto.MainActivity;
import app.pasto.org.pasto.model.User;
import shinobi.kickstart.android.util.SharedPrefUtil;

public class UserManager {
    //There should be a way to get SharedPref without using Context
    public User getUserFromSharedPref(Context context) {
        Gson gson = new Gson();
        String jsonString = SharedPrefUtil.getInstance().getJsonString("user", context);
        return gson.fromJson(jsonString, User.class);
    }

    public void logout(Context context) {
        SharedPreferences.Editor editor = SharedPrefUtil.getInstance().getEditor(context);
        editor.remove("user");
        editor.commit();
    }
}
