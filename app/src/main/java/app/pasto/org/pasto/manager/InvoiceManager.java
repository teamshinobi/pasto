package app.pasto.org.pasto.manager;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.pasto.org.pasto.exception.NoOrdersException;
import app.pasto.org.pasto.exception.OutOfStockException;
import app.pasto.org.pasto.model.Food;
import app.pasto.org.pasto.model.Invoice;
import app.pasto.org.pasto.model.Order;
import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.model.User;

public class InvoiceManager {

    public Invoice createInvoice(List<Food> foodList, Table table, User user) throws NoOrdersException, OutOfStockException {
        List<Food> filteredFoods = filterList(foodList);

        if (filteredFoods.size() < 1) {
            throw new NoOrdersException("Please place orders before saving.");
        }

        Invoice invoice = new Invoice();
        invoice.setStatus("Active");
        invoice.setPaymentType("Cash");
        invoice.setTotal(getTotal(filteredFoods));
        invoice.setUser(user);
        invoice.setTable(table);
        invoice.setOrders(createOrders(filteredFoods));

        return invoice;
    }

    public List<Order> createOrders(List<Food> filteredFoods) {
        List<Order> orders = new ArrayList<Order>();

        for (Food food: filteredFoods) {
            Order order = new Order();
            order.setQuantity(food.getQuantityOrdered());
            order.setTotal(food.getSubTotal());
            order.setFood(food);

            orders.add(order);
        }

        return orders;
    }

    protected double getTotal(List<Food> filteredFoods) {
        double total = 0.0;

        for (Food food: filteredFoods) {
            total += food.getSubTotal();
        }

        return total;
    }

    public List<Food> filterList(List<Food> foodList) throws OutOfStockException{
        List<Food> filtered = new ArrayList<Food>();

        for (Food food: foodList) {
            if (food.getInventoryQuantity() < food.getQuantityOrdered()) {
                throw new OutOfStockException("One of the orders exceeded the available quantity.");
            }
            if (food.getQuantityOrdered() > 0) {
                filtered.add(food);
            }
        }

        return filtered;
    }
}
