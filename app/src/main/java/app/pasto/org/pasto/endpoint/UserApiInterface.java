package app.pasto.org.pasto.endpoint;

import app.pasto.org.pasto.response.UserResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserApiInterface {
    @FormUrlEncoded
    @POST("user/login-check")
    Call<UserResponse> login(@Field("username") String username, @Field("password") String password);
}

