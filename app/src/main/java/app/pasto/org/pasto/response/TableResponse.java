package app.pasto.org.pasto.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.pasto.org.pasto.model.Table;

public class TableResponse
{
    @SerializedName("tables")
    private List<Table> tables;

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }
}
