package app.pasto.org.pasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import app.pasto.org.pasto.endpoint.UserApiInterface;
import app.pasto.org.pasto.model.User;
import app.pasto.org.pasto.response.UserResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import shinobi.kickstart.android.http.ApiClient;
import shinobi.kickstart.android.util.SharedPrefUtil;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.btnLogin)
    Button btnLogin;

    @Bind(R.id.etUsername)
    EditText etUsername;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.ivLogo)
    ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnLogin)
    public void login(View view)
    {
        this.loginUser();
    }

    @OnClick(R.id.ivLogo)
    public void ipChange(View view)
    {

    }


    protected void loginUser() {
        UserApiInterface userApiInterface =
                ApiClient.getClient().create(UserApiInterface.class);

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        Call<UserResponse> call = userApiInterface.login(username, password);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.body().getUser() != null) {
                    User user = response.body().getUser();
                    navigate(user);
                } else {
                    showInvalidCredentials();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
    }

    protected void navigate(User user) {
        SharedPrefUtil.getInstance().writeObject("user", user, this);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    protected void showInvalidCredentials() {
        Toast.makeText(this, "Invalid Credentials.", Toast.LENGTH_SHORT).show();
    }
}
