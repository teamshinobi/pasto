package app.pasto.org.pasto.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import app.pasto.org.pasto.R;
import app.pasto.org.pasto.listener.AddFoodListener;
import app.pasto.org.pasto.listener.AddSubtractFoodListener;
import app.pasto.org.pasto.model.Food;
import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.util.AddOrderInputDialog;
import butterknife.Bind;
import butterknife.ButterKnife;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.CustomViewHolder> {

    Context context;
    LayoutInflater inflater;

    List<Food> data;

    public FoodAdapter(Context context, List<Food> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public FoodAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_food, parent, false);
        FoodAdapter.CustomViewHolder holder = new FoodAdapter.CustomViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FoodAdapter.CustomViewHolder holder, int position) {
        final Food food = data.get(position);
        holder.tvFoodName.setText(food.getName() + " (Php " + food.getPrice() + ")");
        holder.tvCategoryName.setText(food.getCategory());
        holder.tvStockQty.setText("Stocks: " + food.getInventoryQuantity() + "");

        holder.btnMinus.setOnClickListener(new AddSubtractFoodListener(food, holder.tvQuantity, holder.tvSubtotal));
        holder.btnAdd.setOnClickListener(new AddFoodListener(food, holder.tvQuantity, holder.tvSubtotal, context));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvFoodname)
        TextView tvFoodName;

        @Bind(R.id.tvCategoryName)
        TextView tvCategoryName;

        @Bind(R.id.btnAdd)
        Button btnAdd;

        @Bind(R.id.btnMinus)
        Button btnMinus;

        @Bind(R.id.tvQuantity)
        TextView tvQuantity;

        @Bind(R.id.tvSubtotal)
        TextView tvSubtotal;

        @Bind(R.id.tvStockQty)
        TextView tvStockQty;

        public CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
