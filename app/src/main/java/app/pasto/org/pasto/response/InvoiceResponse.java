package app.pasto.org.pasto.response;

import com.google.gson.annotations.SerializedName;

public class InvoiceResponse {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
