package app.pasto.org.pasto.response;

import com.google.gson.annotations.SerializedName;

import app.pasto.org.pasto.model.User;

public class UserResponse {

    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
