
package app.pasto.org.pasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import app.pasto.org.pasto.adapter.TableAdapter;
import app.pasto.org.pasto.endpoint.TableApiInterface;
import app.pasto.org.pasto.manager.UserManager;

import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.model.User;
import app.pasto.org.pasto.response.TableResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import shinobi.kickstart.android.http.ApiClient;
import shinobi.kickstart.android.util.gesturedetector.AdapterClickListener;
import shinobi.kickstart.android.util.gesturedetector.RecyclerViewGestureDetector;

public class MainActivity extends AppCompatActivity implements AdapterClickListener {

    @Bind(R.id.rvTables)
    RecyclerView rvTables;

    @Bind(R.id.tvFullName)
    TextView tvFullName;

    @Bind(R.id.tvRestaurant)
    TextView tvRestaurant;

    @Bind(R.id.btnLogout)
    Button btnLogout;

    private User user;

    private List<Table> tables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_content);
        getUser();
        fetchTables();
    }

    protected void fetchTables() {
        TableApiInterface tableApiInterface =
                ApiClient.getClient().create(TableApiInterface.class);

        Call<TableResponse> call = tableApiInterface.fetchTables(this.user.getRestaurant().getId());
        call.enqueue(new Callback<TableResponse>() {
            @Override
            public void onResponse(Call<TableResponse> call, Response<TableResponse> response) {
                updateUI(response.body().getTables());
            }

            @Override
            public void onFailure(Call<TableResponse> call, Throwable t) {
            }
        });
    }

    protected void updateUI(List<Table> tables) {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tvFullName.setText(user.toString());
        tvRestaurant.setText(user.getRestaurant().getName());

        TableAdapter adapter = new TableAdapter(this, tables);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        rvTables.setLayoutManager(layoutManager);
        rvTables.setAdapter(adapter);

        RecyclerViewGestureDetector detector = new RecyclerViewGestureDetector(this, rvTables, this);
        rvTables.addOnItemTouchListener(detector);

        adapter.notifyDataSetChanged();

        this.tables = tables;
    }

    protected void getUser()
    {
        this.user = new UserManager().getUserFromSharedPref(this);
    }

    @Override
    public void onClick(View v, int position) {
        Table table = this.tables.get(position);
        if (!table.hasActiveOrder()) {
            Intent intent = new Intent(this, TakeOrdersActivity.class);
            intent.putExtra("table", table);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ViewOrderActivity.class);
            intent.putExtra("table", table);
            startActivity(intent);
        }
    }

    @Override
    public void onLongClick(View v, int position) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this.tables != null) {
            setContentView(R.layout.loading_content);
            fetchTables();
        }
    }

    @OnClick(R.id.btnLogout)
    public void logout(View view)
    {
        new UserManager().logout(this);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
