package app.pasto.org.pasto.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import app.pasto.org.pasto.R;
import app.pasto.org.pasto.model.Food;
import app.pasto.org.pasto.model.Order;
import butterknife.Bind;
import butterknife.ButterKnife;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.CustomViewHolder>{

    Context context;

    LayoutInflater inflater;

    List<Order> data;

    public OrderAdapter(Context context, List<Order> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_order, parent, false);
        OrderAdapter.CustomViewHolder holder = new OrderAdapter.CustomViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Order order = this.data.get(position);

        holder.tvFoodName.setText(order.getFood().getName());
        holder.tvQuantity.setText("Quantity: " + order.getQuantity() + "");
        holder.tvSubtotal.setText("Subtotal: " + order.getTotal() + "");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvFoodname)
        TextView tvFoodName;

        @Bind(R.id.tvQuantity)
        TextView tvQuantity;

        @Bind(R.id.tvSubtotal)
        TextView tvSubtotal;

        public CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
