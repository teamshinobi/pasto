package app.pasto.org.pasto.listener;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import app.pasto.org.pasto.model.Food;
import app.pasto.org.pasto.util.AddOrderInputDialog;

public class AddFoodListener implements View.OnClickListener {

    private Food food;
    private TextView tvQuantity, tvSubtotal;
    private Context context;

    public AddFoodListener(Food food, TextView tvQuantity, TextView tvSubtotal, Context context) {
        this.food = food;
        this.tvQuantity = tvQuantity;
        this.tvSubtotal = tvSubtotal;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        AddOrderInputDialog.build(food, context, tvQuantity, tvSubtotal);
    }
}
