package app.pasto.org.pasto.exception;


public class OutOfStockException extends Exception{
    public OutOfStockException(String message) {
        super(message);
    }
}
