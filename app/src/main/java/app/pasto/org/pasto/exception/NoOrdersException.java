package app.pasto.org.pasto.exception;

public class NoOrdersException extends Exception {
    public NoOrdersException(String message) {
        super(message);
    }
}
