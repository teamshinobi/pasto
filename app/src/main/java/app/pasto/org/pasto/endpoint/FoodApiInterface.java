package app.pasto.org.pasto.endpoint;


import app.pasto.org.pasto.response.FoodResponse;
import app.pasto.org.pasto.response.TableResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FoodApiInterface {
    @GET("food/{restaurantId}")
    Call<FoodResponse> fetchFoods(@Path("restaurantId") String restaurantId);
}
