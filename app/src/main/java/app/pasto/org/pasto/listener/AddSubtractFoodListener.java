package app.pasto.org.pasto.listener;

import android.view.View;
import android.widget.TextView;

import app.pasto.org.pasto.R;
import app.pasto.org.pasto.model.Food;

public class AddSubtractFoodListener implements View.OnClickListener {

    private Food food;

    private TextView tvQuantity, tvSubtotal;

    public AddSubtractFoodListener(Food food, TextView tvQuantity, TextView tvSubtotal) {
        this.food = food;
        this.tvQuantity = tvQuantity;
        this.tvSubtotal = tvSubtotal;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAdd) {
            food.addQuantity();
        } else {
            if (food.getQuantityOrdered() > 0) {
                food.subtractQuantity();
            }
        }

        food.computeSubTotal();

        tvQuantity.setText("Quantity: " + food.getQuantityOrdered());
        this.tvSubtotal.setText("Subtotal: " + food.getSubTotal());
    }
}
