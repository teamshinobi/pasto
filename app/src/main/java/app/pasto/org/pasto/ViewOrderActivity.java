package app.pasto.org.pasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.pasto.org.pasto.adapter.FoodAdapter;
import app.pasto.org.pasto.adapter.OrderAdapter;
import app.pasto.org.pasto.endpoint.InvoiceApiInterface;
import app.pasto.org.pasto.model.Invoice;
import app.pasto.org.pasto.model.Order;
import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.response.ActiveInvoiceResponse;
import app.pasto.org.pasto.response.InvoiceResponse;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shinobi.kickstart.android.http.ApiClient;

public class ViewOrderActivity extends AppCompatActivity {

    @Bind(R.id.tvId)
    TextView tvId;

    @Bind(R.id.tvTotal)
    TextView tvTotal;

    @Bind(R.id.rvOrder)
    RecyclerView rvOrder;

    @Bind(R.id.btnAddOrder)
    Button btnAddOrder;

    @Bind(R.id.btnCancel)
    Button btnCancel;

    private Invoice invoice;

    private Table table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_content);
        getInvoice();
    }

    protected void getInvoice() {
        Table table = (Table) getIntent().getSerializableExtra("table");
        this.table = table;

        InvoiceApiInterface invoiceApiInterface =
                ApiClient.getClient().create(InvoiceApiInterface.class);
        Call<ActiveInvoiceResponse> call = invoiceApiInterface.getInvoice(table.getId());
        call.enqueue(new Callback<ActiveInvoiceResponse>() {
            @Override
            public void onResponse(Call<ActiveInvoiceResponse> call, Response<ActiveInvoiceResponse> response) {
                Invoice invoice = response.body().getInvoice();
                updateUI(invoice);
            }

            @Override
            public void onFailure(Call<ActiveInvoiceResponse> call, Throwable t) {

            }
        });
    }

    protected void updateUI(Invoice invoice) {
        setContentView(R.layout.activity_view_order);
        ButterKnife.bind(this);

        this.invoice = invoice;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvId.setText("Invoice #: " + invoice.getId());
        tvTotal.setText("Total: " + invoice.getTotal());

        List<Order> orders  = invoice.getOrders();
        OrderAdapter adapter = new OrderAdapter(this, orders);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvOrder.setLayoutManager(layoutManager);
        rvOrder.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btnAddOrder)
    public void addOrder(View view)
    {
        Intent intent = new Intent(this, TakeOrdersActivity.class);
        intent.putExtra("invoice", invoice.getId());
        intent.putExtra("table", this.table);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btnCancel)
    public void cancelInvoice(View view)
    {
        InvoiceApiInterface invoiceApiInterface =
                ApiClient.getClient().create(InvoiceApiInterface.class);
        Call<InvoiceResponse> call = invoiceApiInterface.cancelInvoice(this.invoice.getId());
        call.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                cancel();
            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {

            }
        });
    }

    //LOL
    protected void cancel() {
        Toast.makeText(this, "Order has been cancelled!", Toast.LENGTH_SHORT).show();
        finish();
    }
}
