package app.pasto.org.pasto.endpoint;

import app.pasto.org.pasto.request.AddOrderRequest;
import app.pasto.org.pasto.request.NewInvoiceRequest;
import app.pasto.org.pasto.response.ActiveInvoiceResponse;
import app.pasto.org.pasto.response.InvoiceResponse;
;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface InvoiceApiInterface {

    @POST("invoice/new")
    Call<InvoiceResponse> createInvoice(@Body NewInvoiceRequest request);

    @GET("invoice/{tableId}")
    Call<ActiveInvoiceResponse> getInvoice(@Path("tableId") String id);

    @POST("invoice/add-orders")
    Call<InvoiceResponse> addOrders(@Body AddOrderRequest request);

    @POST("invoice/cancel/{invoiceId}")
    Call<InvoiceResponse> cancelInvoice(@Path("invoiceId") String invoiceId);
}
