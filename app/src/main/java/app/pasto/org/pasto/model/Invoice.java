package app.pasto.org.pasto.model;

import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Invoice {

    @SerializedName("id")
    private String id;

    @SerializedName("user")
    private User user;

    @SerializedName("table")
    private Table table;

    @SerializedName("date")
    private String date;

    @SerializedName("total")
    private double total;

    @SerializedName("status")
    private String status;

    @SerializedName("payment_type")
    private String paymentType;

    @SerializedName("orders")
    private List<Order> orders;

    public Invoice() {
        this.orders = new ArrayList<Order>();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

