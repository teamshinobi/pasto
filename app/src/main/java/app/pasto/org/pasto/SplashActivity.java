package app.pasto.org.pasto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import app.pasto.org.pasto.model.User;
import butterknife.Bind;
import butterknife.ButterKnife;
import shinobi.kickstart.android.util.SharedPrefUtil;

public class SplashActivity extends AppCompatActivity {

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        Typeface roboto = Typeface.createFromAsset(this.getAssets(),
                "fonts/Roboto-Bold.ttf");
        tvTitle.setTypeface(roboto);

        LogCheckerRunnable runnable = new LogCheckerRunnable(this);
        Thread logThread = new Thread(runnable);

        logThread.start();
    }

    class LogCheckerRunnable implements Runnable {

        String jsonString = "";

        public LogCheckerRunnable(Context context){
            jsonString = SharedPrefUtil.getInstance().getJsonString("user", context);
        }

        @Override
        public void run() {

            Intent intent = null;

            if(jsonString.equals("")){
                try {
                    //just to imitate delay.
                    Thread.sleep(2000);
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                intent = new Intent(SplashActivity.this, MainActivity.class);
            }

            startActivity(intent);
            finish();
        }
    }
}
