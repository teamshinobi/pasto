package app.pasto.org.pasto.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TextView;

import app.pasto.org.pasto.adapter.FoodAdapter;
import app.pasto.org.pasto.model.Food;

public class AddOrderInputDialog {

    public static void build(final Food food, Context context, final TextView tvQuantity, final TextView tvSubtotal){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Place quantity");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                food.setQuantityOrdered(Integer.parseInt(input.getText().toString()));
                food.computeSubTotal();
                tvQuantity.setText(food.getQuantityOrdered() + "");
                tvSubtotal.setText(food.getSubTotal() + "");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
