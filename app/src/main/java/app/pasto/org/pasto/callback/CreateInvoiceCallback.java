package app.pasto.org.pasto.callback;

import app.pasto.org.pasto.response.InvoiceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shinobi.kickstart.android.http.ApiCallbackListener;

public class CreateInvoiceCallback implements Callback<InvoiceResponse> {

    CreateInvoiceCallbackListener listener;

    public CreateInvoiceCallback(CreateInvoiceCallbackListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
        listener.onResponse(call, response);
    }

    @Override
    public void onFailure(Call<InvoiceResponse> call, Throwable t) {
        listener.onFailure(call, t);
    }
}
