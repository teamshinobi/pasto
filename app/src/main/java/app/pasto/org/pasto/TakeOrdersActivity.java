package app.pasto.org.pasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import app.pasto.org.pasto.adapter.FoodAdapter;
import app.pasto.org.pasto.callback.CreateInvoiceCallback;
import app.pasto.org.pasto.callback.CreateInvoiceCallbackListener;
import app.pasto.org.pasto.endpoint.FoodApiInterface;
import app.pasto.org.pasto.endpoint.InvoiceApiInterface;
import app.pasto.org.pasto.exception.NoOrdersException;
import app.pasto.org.pasto.exception.OutOfStockException;
import app.pasto.org.pasto.manager.InvoiceManager;
import app.pasto.org.pasto.manager.UserManager;
import app.pasto.org.pasto.model.Food;
import app.pasto.org.pasto.model.Invoice;
import app.pasto.org.pasto.model.Order;
import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.model.User;
import app.pasto.org.pasto.request.AddOrderRequest;
import app.pasto.org.pasto.request.NewInvoiceRequest;
import app.pasto.org.pasto.response.FoodResponse;
import app.pasto.org.pasto.response.InvoiceResponse;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shinobi.kickstart.android.http.ApiCallbackListener;
import shinobi.kickstart.android.http.ApiClient;

public class TakeOrdersActivity extends AppCompatActivity {

    @Bind(R.id.rvFoods)
    RecyclerView rvFoods;

    List<Food> foodList;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_content);

        this.user = new UserManager().getUserFromSharedPref(this);
        this.fetchFoods();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.take_orders_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save) {
            createInvoice();
        }

        return super.onOptionsItemSelected(item);
    }


    protected void fetchFoods() {
        FoodApiInterface foodApiInterface =
                ApiClient.getClient().create(FoodApiInterface.class);

        Call<FoodResponse> call = foodApiInterface.fetchFoods(user.getRestaurant().getId());
        call.enqueue(new Callback<FoodResponse>() {
            @Override
            public void onResponse(Call<FoodResponse> call, Response<FoodResponse> response) {
                List<Food> foods = response.body().getFoods();
                updateUI(foods);
            }

            @Override
            public void onFailure(Call<FoodResponse> call, Throwable t) {

            }
        });
    }

    protected void createInvoice() {
        Log.d("JC", "HERE");
        try {
            InvoiceApiInterface invoiceApiInterface =
                    ApiClient.getClient().create(InvoiceApiInterface.class);

            if (getIntent().getStringExtra("invoice") == null) {
                Table table = (Table) getIntent().getSerializableExtra("table");
                Invoice invoice = new InvoiceManager().createInvoice(this.foodList, table, user);
                NewInvoiceRequest request = new NewInvoiceRequest(invoice);

                Call<InvoiceResponse> call = invoiceApiInterface.createInvoice(request);
                CreateInvoiceCallbackListener listener = new CreateInvoiceCallbackListener(this, table);
                call.enqueue(new CreateInvoiceCallback(listener));
            } else {
                AddOrderRequest request = new AddOrderRequest();
                Table table = (Table) getIntent().getSerializableExtra("table");
                request.setInvoiceId(getIntent().getStringExtra("invoice"));

                InvoiceManager manager = new InvoiceManager();
                List<Food> filtered = manager.filterList(this.foodList);
                List<Order> orders = manager.createOrders(filtered);
                request.setOrders(orders);

                Call<InvoiceResponse> call = invoiceApiInterface.addOrders(request);
                CreateInvoiceCallbackListener listener = new CreateInvoiceCallbackListener(this, table);
                call.enqueue(new CreateInvoiceCallback(listener));
            }
        } catch (NoOrdersException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (OutOfStockException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    protected void updateUI(List<Food> foods) {
        setContentView(R.layout.activity_take_orders);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FoodAdapter adapter = new FoodAdapter(this, foods);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvFoods.setLayoutManager(layoutManager);
        rvFoods.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        this.foodList = foods;
    }

}
