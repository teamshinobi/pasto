package app.pasto.org.pasto.callback;


import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import app.pasto.org.pasto.ViewOrderActivity;
import app.pasto.org.pasto.model.Invoice;
import app.pasto.org.pasto.model.Table;
import app.pasto.org.pasto.response.InvoiceResponse;
import retrofit2.Call;
import retrofit2.Response;
import shinobi.kickstart.android.http.ApiCallbackListener;


public class CreateInvoiceCallbackListener {

    private Activity activity;
    private Table table;

    public CreateInvoiceCallbackListener(Activity activity, Table table) {
        this.activity = activity;
        this.table = table;
    }

    public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
        Toast.makeText(this.activity, "Orders successfully saved!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this.activity, ViewOrderActivity.class);
        intent.putExtra("table", this.table);
        activity.startActivity(intent);
        activity.finish();
    }

    public void onFailure(Call<InvoiceResponse> call, Throwable t) {

    }
}
