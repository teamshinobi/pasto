package app.pasto.org.pasto.model;


import android.text.Editable;

import com.google.gson.annotations.SerializedName;

public class Food {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("price")
    private double price;

    @SerializedName("category")
    private String category;

    @SerializedName("inventory_quantity")
    private int inventoryQuantity;

    private transient int quantityOrdered;

    private transient double subTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public void addQuantity() {
        this.quantityOrdered++;
    }

    public void subtractQuantity() {
        this.quantityOrdered--;
    }

    public int getQuantityOrdered() {
        return this.quantityOrdered;
    }

    public void computeSubTotal() {
        this.subTotal = this.getPrice() * this.getQuantityOrdered();
    }

    public double getSubTotal() {
        return this.subTotal;
    }

    public int getInventoryQuantity() {
        return inventoryQuantity;
    }

    public void setInventoryQuantity(int inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }
}
