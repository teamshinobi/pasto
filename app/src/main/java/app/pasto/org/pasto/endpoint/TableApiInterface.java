package app.pasto.org.pasto.endpoint;

import app.pasto.org.pasto.response.TableResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TableApiInterface {
    @GET("table/{restaurantId}")
    Call<TableResponse> fetchTables(@Path("restaurantId") String id);
}
