package app.pasto.org.pasto.response;

import com.google.gson.annotations.SerializedName;

import app.pasto.org.pasto.model.Invoice;

public class ActiveInvoiceResponse {

    @SerializedName("invoice")
    Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
