package app.pasto.org.pasto.request;

import com.google.gson.annotations.SerializedName;


import app.pasto.org.pasto.model.Invoice;

public class NewInvoiceRequest
{
    @SerializedName("invoice")
    private Invoice invoice;

    public NewInvoiceRequest(Invoice invoice) {
        this.invoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
