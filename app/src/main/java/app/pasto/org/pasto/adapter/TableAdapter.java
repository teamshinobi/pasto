package app.pasto.org.pasto.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.pasto.org.pasto.R;
import app.pasto.org.pasto.model.Table;
import butterknife.Bind;
import butterknife.ButterKnife;

public class TableAdapter extends RecyclerView.Adapter<TableAdapter.CustomViewHolder> {

    Context context;
    LayoutInflater inflater;

    List<Table> data;

    public TableAdapter(Context context, List<Table> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public TableAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_table, parent, false);
        CustomViewHolder holder = new CustomViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(TableAdapter.CustomViewHolder holder, int position) {
        Table table = data.get(position);

        holder.tvName.setText(table.getName());

        if (table.hasActiveOrder()) {
            holder.tvHasActiveOrder.setText("Has active order.");
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvName)
        TextView tvName;

        @Bind(R.id.tvHasActiveOrder)
        TextView tvHasActiveOrder;

        public CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
