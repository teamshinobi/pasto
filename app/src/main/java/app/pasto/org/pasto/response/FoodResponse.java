package app.pasto.org.pasto.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.pasto.org.pasto.model.Food;

public class FoodResponse {

    @SerializedName("foods")
    List<Food> foods;

    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }
}
