package app.pasto.org.pasto.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.pasto.org.pasto.model.Order;

public class AddOrderRequest {

    @SerializedName("invoice_id")
    private String invoiceId;

    @SerializedName("orders")
    private List<Order> orders;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
