package shinobi.kickstart.android.application.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import java.util.List;

/**
 * Created by JC on 2/20/2016.
 */
public abstract class ShinobiNavigationDrawerActivity  extends AppCompatActivity {


    private NavigationView navigationView;

    private List<Fragment> navigationFragments;

    private int frameID;


    public final String SELECTED_TAB = "selected";

    private int selectedTab = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setNavigationView(NavigationView navigationView){
        this.navigationView = navigationView;
        onNavigationHeaderSetting(navigationView.getHeaderView(0));
    }

    public void setNavigationFragments(List<Fragment> navigationFragments){
        this.navigationFragments = navigationFragments;
    }

    public void setFrameID(int frameID){
        this.frameID = frameID;
    }

    public void selectTab(int position){
        this.selectedTab = position;
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(frameID, navigationFragments.get(position));
        selectNavigationItem(position);
        fragmentTransaction.commit();
    }

    public int getSelectedNavigationPosition(){
        return this.selectedTab;
    }


    private void selectNavigationItem(int position){
        navigationView.getMenu().getItem(position).setChecked(true);
    }

    public void setUpNavigation(){
        selectTab(selectedTab);
    }

    public abstract void  onNavigationHeaderSetting(View headerLayout);
}
