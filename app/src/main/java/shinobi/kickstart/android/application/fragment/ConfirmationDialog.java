package shinobi.kickstart.android.application.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by JC on 1/20/2016.
 */
public class ConfirmationDialog extends DialogFragment{

    private DialogInterface.OnClickListener pListener;
    private DialogInterface.OnClickListener nListener;

    private boolean hasPositiveButton = true;

    private String negativeText = "CANCEL";

    private String message = "";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(this.message)
                .setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        if(hasPositiveButton == true){
            builder.setPositiveButton("OK", pListener);
        }

        return builder.create();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void disablePositiveAction(){
        this.hasPositiveButton = false;
    }

    public void setPositiveAction(DialogInterface.OnClickListener listener){
        this.pListener = listener;
    }
    public void setNegativeAction(DialogInterface.OnClickListener listener){
        this.nListener = listener;
    }

    public void setNegativeButtonText(String text){
        this.negativeText = text;
    }
}
