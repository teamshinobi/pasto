package shinobi.kickstart.android.application.fragment;

import android.support.v4.app.Fragment;

import java.util.List;

/**
 * Created by JC on 1/20/2016.
 */
public abstract class DataFragment extends Fragment {
   public abstract void loadData();
}
