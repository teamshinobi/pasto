package shinobi.kickstart.android.application.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * Created by JC on 2/12/2016.
 */
public class SimpleInputDialog extends DialogFragment{

    private DialogInterface.OnClickListener pListener;
    private DialogInterface.OnClickListener nListener;

    private String message = "Message";
    private String negativeText = "Cancel";

    private View dialogView;

    private EditText editText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(this.message)
                .setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setPositiveButton("OK", pListener);

        builder.setView(dialogView);
        return builder.create();
    }


    public void setView(LayoutInflater inflater, int layoutRes, int editTextid){
        dialogView = inflater.inflate(layoutRes, null);
        editText = (EditText) dialogView.findViewById(editTextid);
    }

    public EditText getPrimaryEditText(){
        return this.editText;
    }


    public void setPositiveAction(DialogInterface.OnClickListener listener){
        this.pListener = listener;
    }

    public void setNegativeAction(DialogInterface.OnClickListener listener){
        this.nListener = listener;
    }

    public void setMessage(String message){
        this.message = message;
    }
}
