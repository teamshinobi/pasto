package shinobi.kickstart.android.util.gesturedetector;

import android.view.View;

/**
 * Created by JC on 8/14/2015.
 */
public interface AdapterClickListener {
    public void onClick(View v, int position);
    public void onLongClick(View v, int position);
}
