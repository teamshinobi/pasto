package shinobi.kickstart.android.util;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by JC on 2/7/2016.
 */
public class NotifUtil {
    private static NotifUtil ourInstance = new NotifUtil();

    public static NotifUtil getInstance() {
        if(ourInstance == null){
            ourInstance = new NotifUtil();
        }
        return ourInstance;
    }

    private NotifUtil() {

    }


    public void showSimpleSnackBarMessage(String message, CoordinatorLayout mCoordinatorLayout){
        final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, message , Snackbar.LENGTH_LONG);
        snackbar.setAction("DISMISS", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }
}
