package shinobi.kickstart.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by JC on 2/7/2016.
 */
public class SharedPrefUtil {
    private static SharedPrefUtil ourInstance = new SharedPrefUtil();

    public static SharedPrefUtil getInstance() {
        if(ourInstance == null){
            ourInstance = new SharedPrefUtil();
        }
        return ourInstance;
    }

    private SharedPrefUtil() {

    }

    public SharedPreferences.Editor getEditor(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).edit();
    }

    public SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void writeData(Context context, HashMap<String, String> map){
        SharedPreferences.Editor editor = getEditor(context);
        Iterator it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            editor.putString(pair.getKey().toString(),  pair.getValue().toString());
        }

        editor.apply();
    }

    public void writeObject(String key, Object object, Context context)
    {
        Gson gson = new Gson();
        String jsonObject = gson.toJson(object);
        SharedPreferences.Editor editor = getEditor(context);

        editor.putString(key, jsonObject);
        editor.commit();
    }

    public String getJsonString(String key, Context context)
    {
       return getSharedPreferences(context).getString(key, "");
    }
}
