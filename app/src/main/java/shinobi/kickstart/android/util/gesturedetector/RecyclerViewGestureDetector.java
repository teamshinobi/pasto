package shinobi.kickstart.android.util.gesturedetector;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by JC on 8/15/2015.
 *
 *
 * Listens for touch events in RecyclerViews
 *
 */

public class RecyclerViewGestureDetector implements RecyclerView.OnItemTouchListener{

    //we can actually supply this on constructor
    private GestureDetector gestureDetector;

    public RecyclerViewGestureDetector(Context context, final RecyclerView recyclerView, final AdapterClickListener listener){

        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){

            @Override
            public boolean onDown(MotionEvent e){


                return true;
            }
            @Override
            public boolean onSingleTapUp(MotionEvent e){
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());

                if(child != null && listener != null){
                    listener.onClick(child, recyclerView.getChildAdapterPosition(child));
                }

                return super.onSingleTapConfirmed(e);
            }

            @Override
            public void onLongPress(MotionEvent e){
                super.onLongPress(e);

                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());

                if(child != null && listener != null){
                    listener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        Log.d("Touch: ", "onTouch: " + e);
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
