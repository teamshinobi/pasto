package shinobi.kickstart.android.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.ConnectException;

/**
 * Created by JC on 2/7/2016.
 */
public class WifiUtil {
    private static WifiUtil instance = new WifiUtil();

    private ConnectivityManager connManager;
    private NetworkInfo mWifi;

    public static WifiUtil getInstance() {

        if(instance == null){
            instance = new WifiUtil();
        }

        return instance;
    }

    private WifiUtil() {}

    public boolean isConnected(Context context){
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = connManager.getActiveNetworkInfo();

        if(mWifi != null){
            return true;
        }
        return false;
    }
}
