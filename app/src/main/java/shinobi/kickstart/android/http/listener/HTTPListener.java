package shinobi.kickstart.android.http.listener;

import org.json.JSONObject;

/**
 * Created by JC on 1/15/2016.
 */
public interface HTTPListener {
    public void onHttpRequestStarted();
    public void onHttpRequestFinished(int statusCode, JSONObject object);
    public void onHttpFailure(String message, int statusCode);
}
