package shinobi.kickstart.android.http;


import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import org.json.JSONException;
import org.json.JSONObject;
import shinobi.kickstart.android.application.AppController;
import shinobi.kickstart.android.http.listener.HTTPListener;

/**
 * Created by JC on 1/15/2016.
 */
public class HTTProcess {

    public static final int REQUEST_SUCCESS = 1;
    public static final int REQUEST_RETURNED_NO_DATA = 2;
    public static final int REQUEST_ERROR = 3;
    public static final int RESPONSE_NULL_CODE = -1;
    
    private ProgressDialog dialog;

    private HTTPListener listener;

    private Activity activity;

    public HTTProcess(Activity activity,HTTPListener listener){
        this.listener = listener;
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    public void showpDialog(String message) {
        hidepDialog();
        dialog.setMessage(message);
        dialog.show();
    }

    public void hidepDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    protected Activity getActivity(){ return this.activity; }

    public HTTPListener getListener() {
        return listener;
    }

    public void setListener(HTTPListener listener) {
        this.listener = listener;
    }

    /*=============================================================================================
    |   Make sure that your json response return an object with property of "success" with either true or false.
    |   If the method doesn't see the property then it will throw an exception and calling the listener to
    |   return REQUEST_ERROR.
    |
     */
    protected Response.Listener<JSONObject> getResponseListener() {
        final Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    boolean sucess = jsonObject.getBoolean("success");
                    if (sucess) {
                        listener.onHttpRequestFinished(REQUEST_SUCCESS, jsonObject);
                    } else {
                        listener.onHttpRequestFinished(REQUEST_RETURNED_NO_DATA, jsonObject);
                    }

                } catch (JSONException e){
                    e.printStackTrace();
                    listener.onHttpRequestFinished(REQUEST_ERROR,null);
                }

            }
        };

        return responseListener;
    }

    protected Response.ErrorListener getResponseErrorListener() {
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Shinobi", "Error: " + error.getMessage());

                NetworkResponse response = error.networkResponse;

                if(response != null){
                    listener.onHttpFailure(error.getMessage(),response.statusCode);
                } else {
                    Log.e("Shinobi", "Cannot reach server. Pls. check connection URL.");
                    listener.onHttpFailure("Cannot reach server.", RESPONSE_NULL_CODE);
                }
            }
        };

        return errorListener;
    }

    protected void sendRequest(CustomRequest request){
        listener.onHttpRequestStarted();
        AppController.getInstance().addToRequestQueue(request);
    }
}
