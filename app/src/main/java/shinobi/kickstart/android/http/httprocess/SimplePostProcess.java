package shinobi.kickstart.android.http.httprocess;

import android.app.Activity;

import com.android.volley.Request;

import java.util.HashMap;

import shinobi.kickstart.android.http.CustomRequest;
import shinobi.kickstart.android.http.HTTProcess;
import shinobi.kickstart.android.http.listener.HTTPListener;

/**
 * Created by JC on 2/25/2016.
 */
public class SimplePostProcess  extends HTTProcess {

    public SimplePostProcess(Activity activity, HTTPListener listener) {
        super(activity, listener);
    }

    public void post(HashMap<String, String> maps, String url){
        CustomRequest request = new CustomRequest(Request.Method.POST, url, this.getResponseErrorListener(), this.getResponseListener());
        request.setParams(maps);
        sendRequest(request);
    }
}