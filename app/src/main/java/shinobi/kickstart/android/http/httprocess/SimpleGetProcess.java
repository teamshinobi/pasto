package shinobi.kickstart.android.http.httprocess;

import android.app.Activity;

import com.android.volley.Request;

import shinobi.kickstart.android.http.CustomRequest;
import shinobi.kickstart.android.http.HTTProcess;
import shinobi.kickstart.android.http.listener.HTTPListener;

/**
 * Created by JC on 2/21/2016.
 */
public class SimpleGetProcess extends HTTProcess {

    public SimpleGetProcess(Activity activity, HTTPListener listener) {
        super(activity, listener);
    }

    public void get(String url){
        CustomRequest request = new CustomRequest(Request.Method.GET, url, this.getResponseErrorListener(), this.getResponseListener());
        sendRequest(request);
    }
}
