package shinobi.kickstart.android.http.httprocess;

import android.app.Activity;
import android.util.Log;

import java.util.List;

import shinobi.kickstart.android.http.CustomRequest;
import shinobi.kickstart.android.http.HTTProcess;
import shinobi.kickstart.android.http.listener.HTTPListener;

/**
 * Created by JC on 1/21/2016.
 */
public class BatchProcess extends HTTProcess {

    private int q = 0;

    public BatchProcess(Activity activity, HTTPListener listener) {
        super(activity, listener);
    }

    protected void sendRequests(List<CustomRequest> requestList){
        getListener().onHttpRequestStarted();
        this.q = requestList.size();
        for(CustomRequest r : requestList){
            sendRequest(r);
        }
    }

    /*===========================================
    |   Decrement 1 on request queues number.
    |
    |   return true if the request queue is empty else false.
    |
     */
    public boolean checkRequest(){
        if(q == 0)
            return true;

        q = q - 1;

        return false;
    }
}
