package shinobi.kickstart.android.http;

import app.pasto.org.pasto.response.InvoiceResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by jcfrane on 2/12/17.
 */

public interface ApiCallbackListener {
    public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response);
    public void onFailure(Call<InvoiceResponse> call, Throwable t);
}
