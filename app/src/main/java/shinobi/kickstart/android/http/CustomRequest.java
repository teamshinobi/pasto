package shinobi.kickstart.android.http;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by JC on 8/12/2015.
 */

public class CustomRequest extends Request<JSONObject> {
    Listener<JSONObject> responseListener;
    private Map<String, String> params;

    public CustomRequest(int method, String url, ErrorListener errorLister,
                         Listener<JSONObject> responseListener) {
        super(method, url, errorLister);

        this.responseListener = responseListener;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {

        return params;
    };

    @Override
    protected void deliverResponse(JSONObject response) {
        responseListener.onResponse(response);

    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Log.e("JC", "UnsupportedEncodingException");
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            Log.e("JC", "JSONException");
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
            volleyError = error;
        }

        return volleyError;
    }
}